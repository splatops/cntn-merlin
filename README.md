    # ############################################### #
    # #  splatops/cntn-merlin/README ############### #
    # ############################################# #
    #
    # Brought to you by...
    # 
    # ::::::::::::'#######::'########:::'######::
    # :'##::'##::'##.... ##: ##.... ##:'##... ##:
    # :. ##'##::: ##:::: ##: ##:::: ##: ##:::..::
    # '#########: ##:::: ##: ########::. ######::
    # .. ## ##.:: ##:::: ##: ##.....::::..... ##:
    # : ##:. ##:: ##:::: ##: ##::::::::'##::: ##:
    # :..:::..:::. #######:: ##::::::::. ######::
    # ::::::::::::.......:::..::::::::::......:::
    #
    ##################################################
   

# Containerizing Merlin
SplatOps presents a contribution to the Merlin project for building and running in containers.  All examples use and assume the end user has installed Docker and supporting tools.

Link to the original project: [Merlin on Github](https://github.com/Ne0nd0g/merlin)

## Getting Started
These instructions will walk you through:

* Building the Merlin Docker image
* Running the Merlin Docker image

### Building the Merlin Docker image locally
Clone this repository. Then...
* ```cd cntn-merlin```
* ```docker build -t merlin:latest .```
  
To validate Merlin is in your local container images...
* ```docker image ls | grep merlin```

### Running the Merlin Docker image
The Merlin Docker image runs in a variety of ways.  Since Merlin operates as a client/server model the Docker build is designed to handle both of those cases as well as consideration of local certificate creation.  The modes of operation of the Merlin Docker image are as follows:
* Server with generation of certificates (server-gen) - this mode of operation will start the Merlin server binary and also create appropriate x509 certificates in the `/merlin/bin/data/x509` container path.
* Server with no generation of certificates (server) - this mode of operation will start the Merlin server binary with the expectation that x509 certificates have previously been generated and are in the `/merlin/bin/data/x509` container path.
* Client (client) - this mode of operation will start the Merlin client binary and expects command flags so as to connect to a running instance of Merlin Server.

#### Insights and Explanation Prerequisites for Merlin Server

There are a few things to note when running Merlin as a server in a Docker container with regard to the build and structure that has been defined by this containerization effort.

First, because of the flexibility in Docker networking the default Merlin server port does not need to be changed.  When running Docker containers default ports are not exposed to the host interface stack until they are explicitly defined.  This means that while the container is running Merlin will normally handle data on TCP/443.  However, it is up to the end user at run time to define the port they want to expose externally.  For example: `-p 8080:443` in the Docker command which will publish TCP/8080 and translate that to TCP/443 for traffic ingress.

Second, this project defines build variables in the Dockerfile.  This is a common build pattern.  Some of the environment variables pertain to certificate creation and contain defaults.  Forcing the end user to run a Docker build simply to regenerate certificates with unique values is a container anti-pattern.  With that in mind the examples below showcase how to run the container with an `./opt/variables.txt` available to override the certificate generation variables.  The entrypoint script will detect if this file exists and if it does will source it for variable override.  An example of this is outlined in the examples section.

Third, since we are dealing with generated x.509 certificates some users may want to control the ability to start and stop the Merlin server but preserve key material for reuse during specific deployments or projects.  This is solved for by leveraging Docker volumes or mounts.  The examples showcase how to map the precreated `${M_BIN}/data/log` directory for persistent storage of a generated certificate.  Sane defaults have been provided for certificate generation but most uses will likely want to change locality and organizational information.

#### Example 1: Run Merlin Server and Generate x509 Certificates Interactively (server-gen)

The following is a command to generate certificates for Merlin, start Merlin as a server, use a volume mount to store the generated certificates and expose Merlin on TCP/4443 of all interfaces (0.0.0.0).

```
git clone {this repository}
cd ./cntn-merlin/opt
cp variables.txt.example variables.txt
{edit variables.txt accordingly}
cd ..

docker network create merlin-net

docker build -t merlin:latest .

docker run \
--name merlin-lserver \
--rm -it \
-v x509:/merlin/bin/data/x509 \
-v `pwd`/opt:/merlin/bin/data/opt \
--network merlin-net \
-p 4443:443 \
merlin:latest server-gen -i 0.0.0.0
```

Let's break this example down...

* Clone the `cntn-merlin` repo.
* Switch to the `opt` directory containing the example `variables.txt.example` override file and copy that to `variables.txt` as that is what the entrypoint script looks for.
* Switch back to the root of the `cntn-merlin` repo.
* [OPTIONAL] We are using the `docker network create merlin-net` to create a managed network for Merlin named `merlin-net`.  In our local testing we want to connect a local server and container, on the same machine, to validate everything is working.  If you're deploying this on different physical hosts/nodes you can skip this step as it's not required.  The advantage this brings to local testing is that name resolution will work for the client to the server when done this way.  If you do not create this network you need to remove the `--network` argument from the `docker run` command below.
* The `docker build` command is used to build a container image for Merlin.  This includes grabbing all of the latest Merlin source and building it in the container.  This command does not have to be rerun once Merlin is built.  However to update Merlin to newer builds this command will need to be run again.  We will also show how to pull a Merlin container from Docker Hub which alleviates the need to use this command.  You may, however, want to customize your container builds and it is best practice to know what is running in your containers, regardless.
* The `docker run` command is used to instantiate (or start-up) the container.
* `--name` is used to provide the container an easy to remember and identify name as well as provide easy name resolution in Docker networks.  This flag is optional and Docker will provide a name for you if none is provided.
* `--rm -it` this is a common pattern when running containers interactively.  The `--rm` flag instructs docker to delete the container on shutdown.  This may seem counterintuitive if you're new to containers, however the ultimate goal is to only preserve what we need to start a new container that picks up where we left off.  This is the core tenant of "Cattle, not Pets".  The `-it` flag instructs docker to keep STDIN open (`-i`) and also to allocate a psuedo-TTY (`-t`).  Unlike `--rm` the `-it` is actually two seperate arguments.
* `-v x509:/merlin/bin/data/x509` instructs Docker to create a _volume_ mount.  The volume will be preserved even after the container exits, thus preserving our x.509 certificates.  To show a volume, even after a container has exited use the `docker volume ls` command.
* ```-v `pwd`/opt:/merlin/bin/data/opt``` instructs Docker to create a _bind_ mount.  This means that Docker will mount the local path (`pwd`) plus `/opt`.  If you've cloned the repository to: `/home/user/cntn-merlin` and are currently in that directory Docker will create a bind mount from `/home/user/cntn-merlin/opt` to `/merlin/bin/data/opt` in the running container.  This allows us to easily manipulate our `variables.txt` file without having to rebuild our Docker container to achieve different certificate locality and organization settings during creation.  The file is also preserved on our local filesystem and is not impacted by the container being removed.  This allows you to start your container with predefined variables which can be leveraged in automated deployments of Merlin.
* `-p 4443:443` instructs Docker to expose TCP/4443 from our host TCP/IP stack and map that to TCP/443 within our container.  The running container has no awareness that traffic is coming in on TCP/4443 and this source port can be changed to be any valid port.  Mapping TCP/443 to TCP/443 is not uncommon, but to showcase running Merlin on a different port the example shows different source and destination mappings.
* `merlin:latest server-gen -i 0.0.0.0` instructs Docker to use the local image named `merlin` and since container images can be tagged use the `latest` version of it.  If you built the Merlin image with different tags you will need to change them in your run command accordingly.  The `server-gen` argument is passed into the entrypoint script and instructs the container to start by building the appropriate certificates and store them at the location in the container (`/merlin/bin/data/x509`) that is volume mounted in our example.  Finally our entrypoint scripts globs all additional arguments and passes those to the server binary.  For example you could change the example line to `merlin:latest server-gen --help` and show the Merlin Server binary help output.  The example shows how to bind Merlin to all available interfaces which is only in the context of the container itself and is required to open Merlin up to external connections.

#### Example 2: Run Merlin Client and Connect to Example 1 Server

This example shows how to connect Merlin Client to the Merlin Server running from `Example 1`.  If `Example 1` is not running, start it as described.

Since these examples are spinning up Docker as single containers and using no orchestrator (Swarm | Kubernetes) networking is, by default, attached to the "bridge" network unless you created the network in `Example 1`.  Networking in Docker is an entire subject in itself but here are a few fast and important rules to understand before we launch the client to test.

The Merlin Server is binding to "all" interfaces in the container as well as the host OS.  This means that our ingress will respond on all local interfaces.  However,  by default our Client will also be running in it's own container and if we try to tell it to connect to `localhost` or `127.0.0.1` the container will be trying to connect to itself vs the host OS.  With that in mind we will need to tell the Merlin Client to connect to an active external IP address on the machine or in `Example 1` we created a network which allows us to connect the Merlin Client directly to the Merlin Server, by name resolution (DNS), without having to go to the external interface.

If you did not create the example Docker network and want to connect through the external interface then the IP address you will connect to is the IPv4 or IPv6 address assigned from your LAN.  To find this use your OS commands to display your routable IP address and substitute that address in the example commands.

For example, on an Ubuntu 18.04 host, a wireless interface might be referenced on the system as `wlp4s0`.  To find the internal address use the `ip address` commmand as follows to find the LAN assigned IP address:

```
ip addr | grep wlp
3: wlp4s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    inet 10.50.50.215/24 brd 10.50.50.255 scope global dynamic noprefixroute wlp4s0
```

In this case use `10.50.50.215` when connecting the client to the server.  Remember, the running containers think they are running in their own OS and network needs to be handled accordingly.

This is a visual representation of connecting through your LAN IP:

![Container Networking through External Interface](/diagrams/merlin-net-00.png)

The diagram depicts the _Merlin Client Container_ initiating a connection to `10.50.50.215` on `TCP/4443`.  Since the server configuration assigned the port mapping of `TCP/4443` to `TCP/443` the connection will get mapped to the _Merlin Server Container_.  In this example we don't need to know anything about the internal addressing of the default Docker network.  This example is also more reflective of a real world deployment simply because the _Merlin Client Container_ will almost always be connecting through the external node IP (LAN or WAN).

Note that the default Docker network has IP addressing assigned that is transparent to our example.  But what if it makes sense to simplify testing directly from container to container?

Remember that in `Example 1` there was an optional configuration item to create a Docker network named `merlin-net` via this command: `docker network create merlin-net`.

In this next portion of the example we can see how to engage direct container to container communications without having to know any IPs on our system.  Since it may be easier to visualize this before delving into commands here is a representative diagram:

![Direct Container to Container Networking](/diagrams/merlin-net-01.png)

What has changed?  Instead of going all the way up to the external network interface and using a port mapping to end up from the source _Merlin Client Container_ to the destination _Merlin Server Container_ we have both containers tied to the `merlin-net` bridge we originally created.  The obvious question most people will have at this point is why not just use the `default` bridge as in the first portion of the example?  By default the `default` Docker Bridge does not enable service discovery / DNS.  This means that the names given to the containers will not resolve from one container to the next if they are connected to `default`.  However, for all user defined networks service discovery / DNS *is* enabled.  We also don't have to connect to a Docker provided mapping since both containers are on the same software-defined network.  This means that the _Merlin Server Container_, by default, will be listening on TCP/443 and we can connect to that directly.  We can run our server the exact same way - even leaving the port mapping in our `docker run` command.  This is because even though we don't need it, it doesn't impede our container to container communications.  This example is why understanding container network is important.

To test connecting the _Merlin Client Container_ to the _Merlin Server Container_ in a _Direct Container to Container Networking_ configuration run the following commands:

Docker Merlin Server Container Instantiation:
```
docker run \
--name merlin-lserver \
--rm -it \
-v x509:/merlin/bin/data/x509 \
-v `pwd`/opt:/merlin/bin/data/opt \
--network merlin-net \
-p 4443:443 \
merlin:latest server -i 0.0.0.0
```
Docker Merlin Client Container Instantiation:
```
docker run \
--name merlin-lclient \
--rm -it \
--network merlin-net \
merlin:latest client -url https://merlin-lserver:443
```
You should note that the _Merlin Server Container_ indicates the client has connected with a log string similar to the following:
```
[+]Received new agent checkin from 40acce35-48b1-40f5-a30e-d8928b1e1426
```
At this point you can run Merlin commands in the _Docker Merlin Server Container_ as normal.

To reiterate this form of testing is only valuable to locally test Merlin.  For real world use cases you will almost always be connecting from different host nodes and thus will need to port map a socket into the Server for the Client to connect to.