#
#
# ############################################### #
# #  splatops/cntn-merlin/Dockerfile ########### #
# ############################################# #
#
# Brought to you by...
# 
# ::::::::::::'#######::'########:::'######::
# :'##::'##::'##.... ##: ##.... ##:'##... ##:
# :. ##'##::: ##:::: ##: ##:::: ##: ##:::..::
# '#########: ##:::: ##: ########::. ######::
# .. ## ##.:: ##:::: ##: ##.....::::..... ##:
# : ##:. ##:: ##:::: ##: ##::::::::'##::: ##:
# :..:::..:::. #######:: ##::::::::. ######::
# ::::::::::::.......:::..::::::::::......:::
#
##################################################
# CONTAINER BASE
FROM golang:1.12-alpine

# VARIABLES
ENV \
    # MERLIN VARIABLES
    M_PATH=/go/src/github.com/Ne0nd0g/merlin \
    M_GHUB=https://github.com/Ne0nd0g/merlin.git \
    M_BIN=/merlin/bin \
    M_LSRV=merlinServer-Linux-x64 \
    M_LAGT=merlinAgent-Linux-x64 \
    M_OVRD=./data/opt/variables.txt \
    # GOLANG VARIABLES
    GOPATH=/go \
    # OPENSSL VARIABLES
    S_BITS=4096 \
    S_DAYS=365 \
    S_KEYOUT=server.key \
    S_CRT=server.crt \
    S_COUNTRY=US \
    S_STATE=Minnesota \
    S_LOCALITY=Minneapolis \
    S_ORGNAME=SplatOps \
    S_ORGUNIT=AllTheOps \
    S_CN=SplatOps.com

# SET PATH AFTER M_BIN
ENV PATH=${PATH}:${M_BIN}

# ENVIRONMENT BUILD
RUN \
echo "### Install dependencies ###" && \
apk add --no-cache --upgrade \
    bash \
    git \
    make \
    openssl \
    p7zip && \
echo "### Setup build structure ###" && \
mkdir -p ${M_PATH} && \
bash -c 'mkdir -p ${M_BIN}/data/{log,x509,opt}' && \
echo "### Retrieve Merlin source ###" && \
git clone ${M_GHUB} ${M_PATH} && \
echo "### Make Merlin ###" && \
cd ${M_PATH} && \
make DIR="${M_BIN}" linux

# CONTAINER STAGING
WORKDIR ${M_BIN}
COPY gen-cert.sh .
COPY entrypoint.sh .
ENTRYPOINT [ "entrypoint.sh" ]
CMD [ "merlinServer-Linux-x64 --help" ]