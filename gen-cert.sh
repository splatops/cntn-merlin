#!/usr/bin/env bash
#
# ############################################### #
# #  splatops/cntn-merlin/gen-cert.sh ########## #
# ############################################# #
#
# Brought to you by...
# 
# ::::::::::::'#######::'########:::'######::
# :'##::'##::'##.... ##: ##.... ##:'##... ##:
# :. ##'##::: ##:::: ##: ##:::: ##: ##:::..::
# '#########: ##:::: ##: ########::. ######::
# .. ## ##.:: ##:::: ##: ##.....::::..... ##:
# : ##:. ##:: ##:::: ##: ##::::::::'##::: ##:
# :..:::..:::. #######:: ##::::::::. ######::
# ::::::::::::.......:::..::::::::::......:::
#
##################################################
#
# VARIABLES (via Dockerfile w/OpenSSL defaults as reference)
#
#   * S_BITS=4096
#   * S_DAYS=365
#   * S_KEYOUT=server.key
#   * S_CRT=server.crt
#   * S_COUNTRY=US
#   * S_STATE=Minnesota
#   * S_LOCALITY=Minneapolis
#   * S_ORGNAME=SplatOps
#   * S_ORGUNIT=AllTheOps
#   * S_CN=SplatOps.com
#
##################################################
set -e

if type "openssl" > /dev/null; then
    openssl req \
        -x509 \
        -nodes \
        -newkey rsa:${S_BITS} \
        -days ${S_DAYS} \
        -keyout ${S_KEYOUT} \
        -out ${S_CRT} \
        -subj "/C=${S_COUNTRY}/ST=${S_STATE}/L=${S_LOCALITY}/O=${S_ORGNAME}/OU=${S_ORGUNIT}/CN=${S_CN}"
    openssl x509 -in server.crt -text -noout
else
    echo "ERROR: openssl not installed [gen-cert.sh]"
fi