#!/usr/bin/env bash
#
# ############################################### #
# #  splatops/cntn-merlin/entrypoint.sh ######## #
# ############################################# #
#
# Brought to you by...
# 
# ::::::::::::'#######::'########:::'######::
# :'##::'##::'##.... ##: ##.... ##:'##... ##:
# :. ##'##::: ##:::: ##: ##:::: ##: ##:::..::
# '#########: ##:::: ##: ########::. ######::
# .. ## ##.:: ##:::: ##: ##.....::::..... ##:
# : ##:. ##:: ##:::: ##: ##::::::::'##::: ##:
# :..:::..:::. #######:: ##::::::::. ######::
# ::::::::::::.......:::..::::::::::......:::
#
##################################################
cd ${M_BIN}

if [ -e ${M_OVRD} ]
then
    echo "INFO: sourced override variables [entrypoint.sh]"
    source ${M_OVRD}
else
    echo "INFO: no override variables found [entrypoint.sh]"
fi

if [[ -z "${@:2}" ]]; then
    echo "INFO: no additional arguments passed, skipping collection [entrypoint.sh]"
else
    M_ARGUMENTS=${@:2}
fi

if [ "$1" = 'server-gen' ]; then
    echo "INFO: generating x509 certificates [entrypoint.sh]"
    cd ${M_BIN}/data/x509
    if [ -f "${M_BIN}/gen-cert.sh" ]; then
        ${M_BIN}/gen-cert.sh
    else
        echo "ERROR: gen-cert.sh does not exist [entrypoint.sh]"
    fi
    echo "INFO: starting server [entrypoint.sh]"
    cd ${M_BIN}
    ${M_LSRV} ${M_ARGUMENTS}

elif [ "$1" = 'server' ]; then
    echo "INFO: starting server [entrypoint.sh]"
    cd ${M_BIN}
    ${M_LSRV} ${M_ARGUMENTS}

elif [ "$1" = 'client' ]; then
    echo "INFO: starting client [entrypoint.sh]"
    cd ${M_BIN}
    ${M_LAGT} ${M_ARGUMENTS}

else
    cd ${M_BIN}
    exec ${@}
fi